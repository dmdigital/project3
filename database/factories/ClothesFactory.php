<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Clothes;
use Faker\Generator as Faker;

$factory->define(Clothes::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'price' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
