<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClothesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clothes_orders', function (Blueprint $table) {
            $table->unsignedbigInteger('clothes_id');
            $table->unsignedbigInteger('order_id');

            $table->unsignedbigInteger('quantity');

            $table->primary(['clothes_id','order_id'], 'clothe_order_pk');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clothes_orders');
    }
}
