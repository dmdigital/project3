<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToClothesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clothes_orders', function (Blueprint $table) {
            //
            // $table
            //     ->foreign('invoice_id', 'invoice_invoiceitem_invoice_id_invoices_id_fk')
            //     ->references('id')
            //     ->on('invoices')
            //     ->onUpdate('RESTRICT')
            //     ->onDelete('RESTRICT');            
            //$table->foreignId('clothe_id')->constrained('clothes');
            //
            $table
                ->foreign('clothe_id', 'clothes_orders_clothe_id_clothes_id_fk')
                ->references('id')
                ->on('clothes')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');        
            //$table->foreignId('order_id')->constrained('orders');
            $table
                ->foreign('order_id', 'clothes_orders_order_id_orders_id_fk')
                ->references('id')
                ->on('orders')
                ->onUpdate('RESTRICT')
                ->onDelete('RESTRICT');               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clothes_orders', function (Blueprint $table) {
            //
        });
    }
}
