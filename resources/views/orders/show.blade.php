@extends('layouts.app')

@section('content')
     <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('orders.index') }}">Order</a>
            </li>
            <li class="breadcrumb-item active">Detail</li>
     </ol>
     <div class="container-fluid">
          <div class="animated fadeIn">
                 @include('coreui-templates::common.errors')
                 <div class="row">
                     <div class="col-lg-12">
                         <div class="card">
                             <div class="card-header">
                                 <strong>Details</strong>
                                  <a href="{{ route('orders.index') }}" class="btn btn-dark pull-right">Back</a>
                             </div>
                             <div class="card-body">
                                 @include('orders.show_fields')
                             </div>
                         </div>

                         <div class="card">
                             <div class="card-header">
                                 <i class="fa fa-align-justify"></i>
                                 <strong>Submitted Clothes</strong> 
                                 <a class="pull-right" href="{{ route('orderclothes.create',[$order->id]) }}"><i class="fa fa-plus-square fa-lg"></i></a>                                 
                             </div>
                             <div class="card-body">
                                <div class="table-responsive-sm">
                                    <table class="table table-striped" id="clothes-table">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Price (KSH)</th>
                                                <th>Quantity</th>
                                                <th>Total ({{$order_price}} KSH)</th>
                                                <th colspan="3">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($clothes as $c)
                                            <tr>                                                
                                                <td>{{ $c->name }}</td>
                                                <td>{{ $c->price }}</td>
                                                <td>{{ $c->pivot->quantity }}</td>
                                                <td>{{ $c->price * $c->pivot->quantity}} </td>
                                                <td>
                                                    {!! Form::open(['route' => ['orderclothes.destroy', $c->id], 'method' => 'delete']) !!}
                                                    {!! Form::hidden('order_id',$order->id) !!}
                                                    <div class='btn-group'>                                                        
                                                        <a href="{{ route('orderclothes.edit', [$c ?? ''->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                             </div>
                         </div>
                         <div class="card">
                             <div class="card-header">
                                 <i class="fa fa-align-justify"></i>
                                 <strong>Payments Made</strong>
                                 <a class="pull-right" href="{{ route('orderpayments.create',[$order->id]) }}"><i class="fa fa-plus-square fa-lg"></i></a>                                  
                             </div>
                             <div class="card-body">
                                <div class="table-responsive-sm">
                                    <table class="table table-striped" id="payments-table">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Amount ({{$order_paid}} KSH)</th>                                                                                                
                                                <th colspan="3">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order->payments as $payment)
                                            <tr>
                                                <td>{{ $payment->date }}</td> 
                                                <td>{{ $payment->amount }}</td>                                               
                                                <td>
                                                    {!! Form::open(['route' => ['orderpayments.destroy', $payment->id], 'method' => 'delete']) !!}
                                                    <div class='btn-group'>                                                        
                                                        <a href="{{ route('orderpayments.edit', [$payment->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                                                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                             </div>
                         </div>
                     </div>
                 </div>
          </div>
    </div>
@endsection
