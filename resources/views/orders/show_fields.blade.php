<!-- Status Field -->
<div class="form-group">
    <strong>{!! Form::label('status', 'Status:') !!}</strong>
    {{ $order->status }}
</div>

<!-- Customer Id Field -->
<div class="form-group">
    <strong>{!! Form::label('customer_id', 'Customer Name:') !!}</strong>
    {{ $order->customer->name }}
</div>

<div class="form-group">
    <strong>{!! Form::label('balance', 'Amount Due:') !!}</strong>
    {{ $order_price - $order_paid }} KSH.
</div>

