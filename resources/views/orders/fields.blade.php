<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['Washing' => 'Washing', 'Drying' => 'Drying', 'Delivery' => 'Delivery', 'Complete' => 'Complete'], 
        $order->status, ['placeholder' => 'Pick a Status...','class' => 'form-control'])!!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer:') !!}    
    {!! Form::select('customer_id', $clients, $order->customer_id, ['placeholder' => 'Pick a client type...','class' => 'form-control'])!!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('orders.index') }}" class="btn btn-secondary">Cancel</a>
</div>
