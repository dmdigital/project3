<div class="table-responsive-sm">
    <table class="table table-striped" id="clothes-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($order->clothes as $c)
            <tr>
                <td>{{ $c->name }}</td>
                <td>{{ $c->price }}</td>
                <td>{{ $c->pivot->quantity}}</td>
                <td>{{ $c->price * $c->pivot->quantity ?? ''}} </td>
                <td>
                    {!! Form::open(['route' => ['clothes.destroy', $clothes->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('clothes.show', [$clothes->id]) }}" class='btn btn-ghost-success'><i class="fa fa-eye"></i></a>
                        <a href="{{ route('clothes.edit', [$clothes->id]) }}" class='btn btn-ghost-info'><i class="fa fa-edit"></i></a>
                        {!! Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-ghost-danger', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>