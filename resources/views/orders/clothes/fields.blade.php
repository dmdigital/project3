<!-- Cloth Type -->
<div class="form-group col-sm-6">
    {!! Form::label('cloth_id', 'Type of Clothes:') !!}    
    {!! Form::select('cloth_id', $clothes, $cloth_id, ['placeholder' => 'Pick a cloth type...','class' => 'form-control'])!!}
</div>
<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::number('quantity', $quantity, ['class' => 'form-control']) !!}
</div>

{!! Form::hidden('order_id',$order->id) !!}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('orders.show', $order->id) }}" class="btn btn-secondary">Cancel</a>
</div>
