




<li class="nav-item {{ Request::is('users*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('users.index') }}">
        <i class="fa fa-users"></i>
        <span>Users</span>
    </a>
</li>

<li class="nav-item {{ Request::is('clothes*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('clothes.index') }}">
        <i class="fa fa-square"></i>
        <span>Clothes</span>
    </a>
</li>
<li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('orders.index') }}">
        <i class="fa fa-tasks"></i>
        <span>Orders</span>
    </a>
</li>
<!-- <li class="nav-item {{ Request::is('payments*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('payments.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Payments</span>
    </a>
</li>
<li class="nav-item {{ Request::is('roles*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('roles.index') }}">
        <i class="nav-icon icon-cursor"></i>
        <span>Roles</span>
    </a>
</li> -->
