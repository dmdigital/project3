<?php

namespace App\Repositories;

use App\Models\Clothes;
use App\Repositories\BaseRepository;

/**
 * Class ClothesRepository
 * @package App\Repositories
 * @version December 19, 2020, 4:52 am UTC
*/

class ClothesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'price'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Clothes::class;
    }
}
