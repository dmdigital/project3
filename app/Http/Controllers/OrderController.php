<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrderController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;
    private $userRepository;

    public function __construct(OrderRepository $orderRepo, UserRepository $userRepo)
    {
        $this->orderRepository = $orderRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $orders = $this->orderRepository->all();
        $users = $this->userRepository->all();

        $users = $this->getdropdownData($users);

        return view('orders.index')
            ->with('orders', $orders)
            ->with('clients',$users);
    }

    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        $order = $this->orderRepository->model();
        $order = new $order;

        $users = $this->userRepository->all();

        $users = $this->getdropdownData($users);

        return view('orders.create')
            ->with('order', $order)
            ->with('clients', $users);
        
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
    public function store(CreateOrderRequest $request)
    {
        $input = $request->all();

        $order = $this->orderRepository->create($input);

        Flash::success('Order saved successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Display the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $order = $this->orderRepository->find($id);

        $clothes = $order->clothes;

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        //Cache::put('order_id', $id);
        session(['order_id' => $id]);

        $order_price = 0;

        foreach ($order->clothes as $c)
        {
            $quantity = $c->pivot->quantity;
            $price = $c->price;

            $order_price += $quantity * $price;            
        }

        $order_paid = 0;

        foreach ($order->payments as $p)
        {
            $order_paid += $p->amount;           
        }
        
        return view('orders.show')
            ->with('clothes', $clothes)
            ->with('order', $order)
            ->with('order_price', $order_price)
            ->with('order_paid', $order_paid);
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->orderRepository->find($id);

        $users = $this->userRepository->all();

        $users = $this->getdropdownData($users);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        return view('orders.edit')->with('order', $order)->with('clients', $users);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $order = $this->orderRepository->update($request->all(), $id);

        Flash::success('Order updated successfully.');

        return redirect(route('orders.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $order = $this->orderRepository->find($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('orders.index'));
        }

        $this->orderRepository->delete($id);

        Flash::success('Order deleted successfully.');

        return redirect(route('orders.index'));
    }
}
