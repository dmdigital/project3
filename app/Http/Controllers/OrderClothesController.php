<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClothesRequest;
use App\Http\Requests\UpdateClothesRequest;
use App\Repositories\ClothesRepository;
use App\Repositories\OrderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class OrderClothesController extends AppBaseController
{
    /** @var  ClothesRepository */
    private $clothesRepository;
    private $orderRepository;

    public function __construct(ClothesRepository $clothesRepo, OrderRepository $orderRepo)
    {
        $this->clothesRepository = $clothesRepo;
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Clothes.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        //Get Order ID from the query string
        $orderID = (int) $_SERVER['QUERY_STRING'];

        $order = $this->orderRepository->find($orderID); 

        $clothes = $order->clothes;
        

        return view('orders.clothes.index')
            ->with('order', $order)
            ->with('clothes', $clothes);
    }

    /**
     * Show the form for creating a new Clothes.
     *
     * @return Response
     */
    public function create()
    {
        //Get Order ID from the query string
        $orderID = (int) $_SERVER['QUERY_STRING'];

        $order = $this->orderRepository->find($orderID);
        
        $clothes = $this->clothesRepository->all();
        $clothes = $this->getdropdownData($clothes);

        $cloth_id = NULL;
        $quantity = 0;

        return view('orders.clothes.create')
                ->with('clothes', $clothes)
                ->with('cloth_id', $cloth_id)
                ->with('quantity', $quantity)
                ->with('order', $order);
    }

    /**
     * Store a newly created Clothes in storage.
     *
     * @param CreateClothesRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $orderID = $request->input('order_id');

        $order = $this->orderRepository->find($orderID);

        $clothID = $request->input('cloth_id');

        $quantity = $request->input('quantity');

        $order->clothes()->attach($clothID,
                                     ['quantity' => $quantity]);

        Flash::success('Clothes added successfully to order number '.$orderID.'.');

        return redirect(route('orders.show', $orderID));
    }

    /**
     * Display the specified Clothes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //Get Order ID from the query string
        $orderID = $request->input('order_id');

        $order = $this->orderRepository->find($orderID);
        
        $clothes = $this->clothesRepository->find($id);
        
        if (empty($clothes)) {
            Flash::error('Clothes not found');

            return redirect(route('order.show', $orderID));
        }

        // $market->bids()->detach($id);

        //$order->clothes()->detach($clothes->id);        

        return redirect(route('order.show', $orderID)); 
    }

    /**
     * Show the form for editing the specified Clothes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $cloth = $this->clothesRepository->find($id);

        $clothes = $this->clothesRepository->all();
        $clothes = $this->getdropdownData($clothes);

        $orderID = session('order_id');

        $order = $this->orderRepository->find($orderID);

        if (empty($cloth)) {
            Flash::error('Clothes not found');

            return redirect(route('order.show', $orderID)); 
        }        

        $quantity = 0;
        foreach ($order->clothes as $c)
        {
            if ($c->id == $id){
                $quantity = $c->pivot->quantity;
                break;
            }
        }


        return view('orders.clothes.edit')
            ->with('clothes', $clothes)
            ->with('cloth', $cloth)
            ->with('cloth_id', $id)
            ->with('quantity', $quantity)
            ->with('order', $order);
    }

    /**
     * Update the specified Clothes in storage.
     *
     * @param int $id
     * @param UpdateClothesRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $clothes = $this->clothesRepository->find($id);

        $orderID = $request->input('order_id');

        $order = $this->orderRepository->find($orderID);

        $quantity = $request->input('quantity');

        if (empty($clothes)) {
            Flash::error('Clothes not found');

            return redirect(route('clothes.index'));
        }

        $order->clothes()->updateExistingPivot($id, [
            'quantity' => $quantity,
        ]);

        Flash::success('Ordered clothes updated successfully.');

        return redirect(route('orders.show', $orderID)); 
    }

    /**
     * Remove the specified Clothes from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clothes = $this->clothesRepository->find($id);

        $orderID = session('order_id');

        $order = $this->orderRepository->find($orderID);

        if (empty($clothes)) {
            Flash::error('Clothes not found');

            return redirect(route('orders.show', $orderID));
        }

        $order->clothes()->detach($id);

        Flash::success('Clothes deleted successfully.');

        return redirect(route('orders.show', $orderID));
    }
}
