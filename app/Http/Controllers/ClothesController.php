<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClothesRequest;
use App\Http\Requests\UpdateClothesRequest;
use App\Repositories\ClothesRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ClothesController extends AppBaseController
{
    /** @var  ClothesRepository */
    private $clothesRepository;

    public function __construct(ClothesRepository $clothesRepo)
    {
        $this->clothesRepository = $clothesRepo;
    }

    /**
     * Display a listing of the Clothes.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $clothes = $this->clothesRepository->all();

        return view('clothes.index')
            ->with('clothes', $clothes);
    }

    /**
     * Show the form for creating a new Clothes.
     *
     * @return Response
     */
    public function create()
    {
        return view('clothes.create');
    }

    /**
     * Store a newly created Clothes in storage.
     *
     * @param CreateClothesRequest $request
     *
     * @return Response
     */
    public function store(CreateClothesRequest $request)
    {
        $input = $request->all();

        $clothes = $this->clothesRepository->create($input);

        Flash::success('Clothes saved successfully.');

        return redirect(route('clothes.index'));
    }

    /**
     * Display the specified Clothes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $clothes = $this->clothesRepository->find($id);

        if (empty($clothes)) {
            Flash::error('Clothes not found');

            return redirect(route('clothes.index'));
        }

        return view('clothes.show')->with('clothes', $clothes);
    }

    /**
     * Show the form for editing the specified Clothes.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clothes = $this->clothesRepository->find($id);

        if (empty($clothes)) {
            Flash::error('Clothes not found');

            return redirect(route('clothes.index'));
        }

        return view('clothes.edit')->with('clothes', $clothes);
    }

    /**
     * Update the specified Clothes in storage.
     *
     * @param int $id
     * @param UpdateClothesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClothesRequest $request)
    {
        $clothes = $this->clothesRepository->find($id);

        if (empty($clothes)) {
            Flash::error('Clothes not found');

            return redirect(route('clothes.index'));
        }

        $clothes = $this->clothesRepository->update($request->all(), $id);

        Flash::success('Clothes updated successfully.');

        return redirect(route('clothes.index'));
    }

    /**
     * Remove the specified Clothes from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clothes = $this->clothesRepository->find($id);

        if (empty($clothes)) {
            Flash::error('Clothes not found');

            return redirect(route('clothes.index'));
        }

        $this->clothesRepository->delete($id);

        Flash::success('Clothes deleted successfully.');

        return redirect(route('clothes.index'));
    }
}
